define(`DESTPATH', `const $2Path = "$1/$2/";')dnl
define(`GULPTASK', `gulp.task("$1", ["scss-$2", "html-$2", "fonts-$2", "images-$2", "lint", "js-$2"]);')dnl
define(`GULPDEST', `dest($1Path')dnl
"use strict";

const gulp = require("gulp");
const sass = require("gulp-sass");
const sourcemaps = require("gulp-sourcemaps");
const cleanCss = require("gulp-clean-css");
const htmlMinifier = require("gulp-html-minifier");
const uglify = require("gulp-uglify");
const concat = require("gulp-concat");
const rename = require("gulp-rename");
const eslint = require("gulp-eslint");

DESTPATH(OBJ, ENV)
const scssSrc = "./sass/*.scss";
const cssDir = "css/";
const fontsDir = "fonts/";
const fontsGlob = fontsDir + "**/*";
const imagesDir = "images/";
const imagesGlob = imagesDir + "**/*";
const jsDir = "js/";
const jsGlob = jsDir + "*.js";
const htmlGlob = "./*.html";

const jsCompileMap = {
  "js/jquery.js": ["node_modules/jquery/dist/jquery.min.js"],
  "js/vue.js": ["node_modules/vue/dist/vue.min.js"],
  "js/highlight.js": ["node_modules/highlightjs/highlight.pack.min.js"],
  "js/chartist.js": ["node_modules/chartist/dist/chartist.min.js"],
  "js/login.js": [
    "js/constants.js",
    "js/utils.js",
    "js/http.js",
    "js/auth-common.js",
    "js/login.js"
  ],
  "js/forgot.js": [
    "js/constants.js",
    "js/utils.js",
    "js/http.js",
    "js/forgot.js"
  ],
  "js/reset.js": [
    "js/constants.js",
    "js/utils.js",
    "js/http.js",
    "js/reset.js"
  ],
  "js/signup.js": [
    "js/constants.js",
    "js/utils.js",
    "js/http.js",
    "js/auth-common.js",
    "js/signup.js"
  ],
  "js/dashboard.js": [
    "js/constants.js",
    "js/utils.js",
    "js/http.js",
    "js/errors.js",
    "js/self.js",
    "js/dashboard.js",
    "js/dashboard-setting.js",
    "js/dashboard-domain.js",
    "js/dashboard-installation.js",
    "js/dashboard-general.js",
    "js/dashboard-moderation.js",
    "js/dashboard-statistics.js",
    "js/dashboard-import.js",
    "js/dashboard-danger.js",
    "js/dashboard-export.js",
  ],
  "js/logout.js": [
    "js/constants.js",
    "js/utils.js",
    "js/logout.js"
  ],
  "js/commento.js": ["js/commento.js"],
  "js/count.js": ["js/count.js"],
  "js/unsubscribe.js": [
    "js/constants.js",
    "js/utils.js",
    "js/http.js",
    "js/unsubscribe.js",
  ],
};

gulp.task("scss-verbose", function () {
  return gulp.src(scssSrc)
    .pipe(sourcemaps.init())
    .pipe(sass({outputStyle: "expanded"}).on("error", sass.logError))
    .pipe(sourcemaps.write())
    .pipe(gulp.GULPDEST(ENV) + cssDir));
});

gulp.task("scss-quiet", function () {
  return gulp.src(scssSrc)
    .pipe(sass({outputStyle: "compressed"}).on("error", sass.logError))
    .pipe(cleanCss({compatibility: "ie8", level: 2}))
    .pipe(gulp.GULPDEST(ENV) + cssDir));
});

gulp.task("html-verbose", function () {
  gulp.src([htmlGlob]).pipe(gulp.GULPDEST(ENV)));
});

gulp.task("html-quiet", function () {
  gulp.src(htmlGlob)
    .pipe(htmlMinifier({collapseWhitespace: true, removeComments: true}))
    .pipe(gulp.GULPDEST(ENV)))
});

gulp.task("fonts-verbose", function () {
  gulp.src([fontsGlob]).pipe(gulp.GULPDEST(ENV) + fontsDir));
});

gulp.task("fonts-quiet", function () {
  gulp.src([fontsGlob]).pipe(gulp.GULPDEST(ENV) + fontsDir));
});

gulp.task("images-verbose", function () {
  gulp.src([imagesGlob]).pipe(gulp.GULPDEST(ENV) + imagesDir));
});

gulp.task("images-quiet", function () {
  gulp.src([imagesGlob]).pipe(gulp.GULPDEST(ENV) + imagesDir));
});

gulp.task("js-verbose", function () {
  for (let outputFile in jsCompileMap) {
    gulp.src(jsCompileMap[outputFile])
      .pipe(sourcemaps.init())
      .pipe(concat(outputFile))
      .pipe(rename(outputFile))
      .pipe(sourcemaps.write())
      .pipe(gulp.GULPDEST(ENV)))
  }
});

gulp.task("js-quiet", function () {
  for (let outputFile in jsCompileMap) {
    gulp.src(jsCompileMap[outputFile])
      .pipe(concat(outputFile))
      .pipe(rename(outputFile))
      .pipe(uglify())
      .pipe(gulp.GULPDEST(ENV)))
  }
});

gulp.task("lint", function () {
  return gulp.src(jsGlob)
    .pipe(eslint())
    .pipe(eslint.failAfterError())
});

GULPTASK(ENV, INFO)
