package main

import (
	"commento/api"
)

func main() {
	api.Init()

	api.ExitIfError(api.RoutesServe())
}
