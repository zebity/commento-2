//
// File: commento/smtpex.go
//
// Purpose: the net/smtp package SendMail function generate a failure with
//	postfix MTA, due to using localhost in EHLO greeting.
//	This is a quick hack to get around this by addting ControlOptions
//	parameter to function allow use of FQDN rather than localhost
//	All copyright notices from orginal golang net/smtp.go implementation
//	have been left intact
//
// The hacked function to use as alternate wrapped SendMailEx is available for
// all and any to use with no incumbances,
//

// Copyright 2010 The Go Authors. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.
// Package smtp implements the Simple Mail Transfer Protocol as defined in RFC 5321.
// It also implements the following extensions:
//	8BITMIME  RFC 1652
//	AUTH      RFC 2554
//	STARTTLS  RFC 3207
// Additional extensions may be handled by clients.
//
// The smtp package is frozen and is not accepting new features.
// Some external packages provide more functionality. See:
//
//   https://godoc.org/?q=smtp

package api

import (
	"crypto/tls"
	"errors"
	"net"
	"net/smtp"
	"strings"
)

type CtlOptions struct {
	useFQDN bool
	FQDN string
	localName string
}

func NewCtlOptions(fqdn string, loc string)  *CtlOptions {
	var u bool = true
	if fqdn == "" {
		u = false
	}
	return &CtlOptions{u, fqdn, loc}
}

func (o *CtlOptions) HelloName() string {
	if o.useFQDN {
		return o.FQDN
	}
	return o.localName
}

// SendMail connects to the server at addr, switches to TLS if
// possible, authenticates with the optional mechanism a if possible,
// and then sends an email from address from, to addresses to, with
// message msg.
// The addr must include a port, as in "mail.example.com:smtp".
//
// The addresses in the to parameter are the SMTP RCPT addresses.
//
// The msg parameter should be an RFC 822-style email with headers
// first, a blank line, and then the message body. The lines of msg
// should be CRLF terminated. The msg headers should usually include
// fields such as "From", "To", "Subject", and "Cc".  Sending "Bcc"
// messages is accomplished by including an email address in the to
// parameter but not including it in the msg headers.
//
// The SendMail function and the net/smtp package are low-level
// mechanisms and provide no support for DKIM signing, MIME
// attachments (see the mime/multipart package), or other mail
// functionality. Higher-level packages exist outside of the standard
// library.

func SendMailEx(addr string, a smtp.Auth, from string, to []string, msg []byte, ops *CtlOptions) error {
	host,_,_ := net.SplitHostPort(addr)
	if err := ValidateLine(from); err != nil {
		return err
	}
	for _, recp := range to {
		if err := ValidateLine(recp); err != nil {
			return err
		}
	}
	c, err := smtp.Dial(addr)
	if err != nil {
		return err
	}
	defer c.Close()
	if err = c.Hello(ops.HelloName()); err != nil {
		return err
	}
	if ok, _ := c.Extension("STARTTLS"); ok {
		config := &tls.Config{ServerName: host}
		if err = c.StartTLS(config); err != nil {
			return err
		}
	}
	if a != nil {
		if ok, _ := c.Extension("AUTH"); !ok {
			return errors.New("smtp: server doesn't support AUTH")
		}
		if err = c.Auth(a); err != nil {
			return err
		}
	}
	if err = c.Mail(from); err != nil {
		return err
	}
	for _, addr := range to {
		if err = c.Rcpt(addr); err != nil {
			return err
		}
	}
	w, err := c.Data()
	if err != nil {
		return err
	}
	_, err = w.Write(msg)
	if err != nil {
		return err
	}
	err = w.Close()
	if err != nil {
		return err
	}
	return c.Quit()
}
// validateLine checks to see if a line has CR or LF as per RFC 5321
func ValidateLine(line string) error {
	if strings.ContainsAny(line, "\n\r") {
		return errors.New("smtp: A line must not contain CR or LF")
	}
	return nil
}
