package api

func Init() error {
	ExitIfError(loggerCreate())
	ExitIfError(versionPrint())
	ExitIfError(configParse())
	ExitIfError(dbConnect(5))
	ExitIfError(migrate())
	ExitIfError(smtpConfigure())
	ExitIfError(smtpTemplatesLoad())
	ExitIfError(oauthConfigure())
	ExitIfError(markdownRendererCreate())
	ExitIfError(emailNotificationPendingResetAll())
	ExitIfError(emailNotificationBegin())
	ExitIfError(sigintCleanupSetup())
	ExitIfError(versionCheckStart())
	ExitIfError(domainExportCleanupBegin())
	ExitIfError(viewsCleanupBegin())
	ExitIfError(ssoTokenCleanupBegin())
	return nil
}
