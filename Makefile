#
# Targets == directories
#
# api == go package
# commenta == go main
# db == sql scripts
# templates == 
# frontend == Web UI using nodejs
# scripts ==
#
# build targets: build install dist fmt test clean uninstall
#

SHELL = bash
MAKE = make
TAR = tar
GO = go
GO-COMPILE = go tool 
GO-LIB = go tool compile -pack
GO-CLEAN = go clean
OBJ = _obj
CD = cd
MKDIR = mkdir
RM = rm

ifeq ($(strip $(GOPATH)),)
  ENV = ERR-NO-GOPATH
endif

ifeq ($(strip $(ENV)),)
  ENV = prod
endif

ROOT_DIR = $(shell dirname $(realpath $(lastword $(MAKEFILE_LIST))))

OBJ_DIR = $(ROOT_DIR)/$(OBJ)
COMMENTO_DIST_DIR = commento
DIST_DIR = $(OBJ_DIR)/$(ENV)
TARGET_DIR = $(OBJ_DIR)/$(ENV)/$(COMMENTO_DIST_DIR)
INSTALL_DIR = /usr/local/bin

target = api commento frontend db templates
TARGET = $(target)

.PHONY: subdirs $(TARGET) all build clean install uninstall dist test fmt

all: checkenv subdirs 
	@echo "INFO: all ($(ENV)) ..."

build: checkenv subdirs
	@echo "INFO: Building ..."

install: checkenv subdirs install-env
	@echo "INFO: Install env - $(ENV)"

uninstall: subdirs rminstall-env
	@echo "INFO: remove installed env - $(ENV)"

dist: checkenv
	@echo "INFO: creating distribution"
	@($(CD) $(DIST_DIR) && sudo chown -R root:root $(COMMENTO_DIST_DIR) && $(TAR) cvf $(COMMENTO_DIST_DIR)-$(ENV)-$(INFO).tar $(COMMENTO_DIST_DIR))

install-env:

clean: subdirs
	@$(RM) -rf $(TARGET_DIR)

subdirs: $(TARGET)

$(TARGET):
	@echo "INFO: Making: $@"
	@$(MAKE) -C $@ $(MAKECMDGOALS) INSTALL_DIR=$(TARGET_DIR)


checkenv:
ifeq ($(ENV),$(filter $(ENV),dev test prod))
	@echo "INFO: target dir ($(TARGET_DIR))"
	@$(MKDIR) -p $(TARGET_DIR) 
else
	@echo "ERR: ENV=(dev|test|prod) only: $(ENV)"
	exit 1
endif


